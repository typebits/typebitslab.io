# Fonts created in the Type:Bits workshops

In this page, we've put together a showcase of all the fonts that have been created in the Type:Bits workshops over the years. Clicking on each image will take you to the GitLab page for that font, where you will find their sources and downloadable font files.

## Barcelona 2014

### Setperset

[![Setperset specimen](img/specimen-setperset.png)](https://gitlab.com/typebits/font-setperset)

### Deuperset

[![Deuperset specimen](img/specimen-deuperset.png)](https://gitlab.com/typebits/font-deuperset)

### Sisperdotze

[![Sisperdotze specimen](img/specimen-sisperdotze.png)](https://gitlab.com/typebits/font-sisperdotze)


## London 2014

### Mozblock

[![Mozblock specimen](img/specimen-mozblock.png)](https://gitlab.com/typebits/font-mozblock)


## Tomelloso 2016

### Avestruz

[![Avestruz specimen](img/specimen-avestruz.png)](https://gitlab.com/typebits/font-avestruz)

### Flamingo

[![Flamingo specimen](img/specimen-flamingo.png)](https://gitlab.com/typebits/font-flamingo)

### Tucan

[![Tucan specimen](img/specimen-tucan.png)](https://gitlab.com/typebits/font-tucan)


## Avilés 2017

### Ladrillo

[![Ladrillo specimen](img/specimen-ladrillo.png)](https://gitlab.com/typebits/font-ladrillo)

### Muralla

[![Muralla specimen](img/specimen-muralla.png)](https://gitlab.com/typebits/font-muralla)

### Persiana

[![Persiana specimen](img/specimen-persiana.png)](https://gitlab.com/typebits/font-persiana)

### Roseta

[![Roseta specimen](img/specimen-roseta.png)](https://gitlab.com/typebits/font-roseta)


## Graz 2018

### Spalte

[![Spalte specimen](img/specimen-spalte.png)](https://gitlab.com/typebits/font-spalte)
