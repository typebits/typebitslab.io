# Principles and methodology

The Type:Bits workshops came about as we thought about ways to deal with (what
we understood as) dilemmas and difficulties of type design workshops:

- The complexity of the subject matter meant that there needs to be a
  substantial amount of time dedicated to design foundations.
- Font editors require a long introduction to their concepts and user
  interfaces.
- Vector drawing is yet another layer of complexity.
- It is hard to target non-designers because of the above complexity aspects.
- The requirement to approach technical knowledge often shadows the freedom
  required for design experimentation.
- The pipeline from initial concept to finished typeface can be quite long, and
  it is really hard to go through everything in short-term workshops.

Over the years, and informed by F/LOSS development guidelines and established
practice, we've developed a methodology for short-duration type design
workshops. Here we will attempt to outline the principles behind the Type:Bits
approach.


## Design in the grid

Editing digital fonts is hard. Introducing people, especially non-designers, to font design therefore needs some care when planning what to approach.

One of the foundations of font design is vector drawing. It's easy to forget, once you know how to do it, how non-intuitive it is as a drawing technique. For a workshop that intends to deal with font design on a beginner level, with a focus on design and experimentation, and not so much technical prowess or skill, we could probably do without having to teach vector editing.

Thankfully, one can find another approach in the history of digital type design before PostScript and vector outline fonts. Bitmap fonts, or pixel fonts, were the norm before complex shapes and curves could be encoded into the digital medium. And as it turns out, they are an excellent alternative to vector fonts for a workshop such as Type:Bits.

Bitmap based fonts are simpler to draw because they're constricted to a grid. This is a handy approach for short time-frames such as a one-day workshop. The square grid also provides a great creative restraint when thinking up design directions. Finally, it helps a lot when it comes to understanding font design rules — ascendants, descendants, baseline, x-height — and makes it easier to keep line widths, curves and slopes coherent across the various glyphs.


## Analogue start

The workshop begins by drawing letters with pencil and paper. 

Method-wise, it is really important to be away from the computer in the early stages of font development. Paper and pen are ideal tools to quickly sketch and test ideas. They're also less biased than a font editor, a vector drawing program or any other kind of digital interface; there is no introduction necessary to these simple drawing tools, and ideas can flow and grow freely this way. 

In a collective setting, drawings are easy to take apart, share with everyone and attach to the wall for a closer look and collective feedback.


## Collaborative design and decision making

Font design is usually presented as a solitary kind of endeavour. One of the goals was to figure out smooth ways to allow for real-time collaborative design.

In the Type:Bits workshops, participants work together in groups of four or more. The aim is to have people talk about their ideas for the font and make joint decisions on which design direction they will go for.

Workshops start with everyone drawing the same characters individually. Afterwards, the moment comes when one of the proposed design directions must be picked so it can be developed further in a group effort.

This decision is a tricky moment as in font design there aren't objectively good or bad designs. Instead it's all about pursuing an idea or a visually pleasing form. With small groups it's easier to try and reach a consensus; if this doesn't work, the workshop facilitators assume the role of benevolent dictators, picking a drawing. The [Benevolent Dictator for Life](https://en.wikipedia.org/wiki/Benevolent_dictator_for_life) (BDFL) is a figure which exists in many Free Software projects; it is the person that makes hard decisions when the community around the project can't come to a consensus. This concept was adopted to reduce the time that collective design critiques necessarily occupy. Besides it is not too problematic since the final design will be open and every participant is free to later pursue a different direction in a fork.


## Quick development

During the workshop, time is split into small sprints. These sprints work well to get people engaged and focused, making it possible to end the workshop with a finished font. There are many glyphs to be drawn, even if the limit is just uppercase letters, numbers and punctuation. It can seem little at first but we're talking about a few dozens of characters that need to be designed and digitised. 

There are several steps involved in the design process: paper drawings, digitisation of these drawings into text files, spacing review, specimen design and quality control. Bugs can lurk in every step of this pipeline, and it is necessary to make peace with the fact that they can't all be squashed on the first run.

Quick development as a method allows people to concentrate on the process and take attention to the main focus — decision making, sharing and tools — instead of losing themselves in the aesthetics or design minutiae. Keeping in mind that great is the enemy of good, and knowing that the design can be further iterated later, the focus is on doing a first realease. Something that's ready to use at the end of the workshop even if at times glitchy.


## Finish with concrete results

Starting a new project is always exciting and motivating, but finishing usually isn't easy. 

There are many reasons why wrapping up a project is hard, and one of them is dealing with all the little decisions involved in the process. It's easy to get tangled in details and let time run out, ending up with an incomplete outcome. There's always takeaways in the process itself, but it's even more rewarding to get to the end with something done to show for it.

That's is why one of the fundamental goals of the Type:Bits workshops is to end each session with at least one finished font. This font will very likely have bugs, missing characters or design decisions people would like to review; nevertheless, it will actually exist as an actual first version that can later be iterated on.

Reaching the workshop deadline with an actual font file also ensures that everyone goes through every single step of the font design process — particularly the actual finishing and publishing of the font.

Again inspired by the Free Software development process: ["Release early, release often."](https://en.wikipedia.org/wiki/Release_early%2C_release_often). The motto translates into making the release the focus thus not allowing participants to be lost in the design process. Instead, they are forced to come to collective conclusions — which can obviously be reviewed after the first release!


## F/LOSS tools, libre licenses

Using free software tools and libre/open licenses is a very important point, as
a way to preserve every participant's ability to keep on working further on
the font, and also have it available for everyone to reuse. This is only
possible if the font allows for modification and redistribution, and if the
tools required to do so are not restricted or closed.

The fonts designed in this workshop are published under the [Open Font License](http://scripts.sil.org/OFL),
which gives everyone the right to study, modify and redistribute the font. All
the tools employed for the workshops are Free/Libre and Open Source Software (F/LOSS).


## Alternative font editors

One common problem in most practice-based workshops that employ digital tools is the installation of those tools. It can take a significant amount of time to get participants to run the necessary software, given that people will be running different operating systems (and different versions of those).

The first attempt to deal with this in our type design workshops was to employ virtual machines: an image of a minimal GNU/Linux distribution was made available, containing the tools and materials for the workshop, as well as a custom user interface that did away with anything that was not relevant to the workshop. This proved to be a good way for participants to get a feel of a GNU/Linux system and the available free software tools. There was one important shortcoming though: sharing files between participants was hard, and we had to resort to email or passing around USB sticks.

But yet another question was itching in our minds: _Can you design fonts without a font editor? If so, what tools can you use?_. At some point, Lasse Fister's Bitmap Font Builder system stumbled in our way. It provided a plaintext workflow for designing fonts. Each glyph is drawn in a plain text file, in a way that's reminiscent of ASCII art and the practice of drawing with text characters. This ended up becoming the answer to our question of whether _editorlessness_ was possible. Everyone knows their way around a text editor, and there was no more issue of going through installing software packages or virtual machines for the workshop to run smoothly. It is also pretty exciting to get into a new way of designing glyphs and explore the affordances that come with it.


## One step further with GitLab

Eventually the _editorless_ approach went one step further and there is no need for any local software, thanks to GitLab. By turning the fonts into Git repositories and posting them on GitLab, participants could directly edit glyphs inside their browser using the GitLab editing interface. Not only did this mean that everyone could share the same workflow without installing anything, it also ensured that we could take advantage of a Git-based workflow without having to teach the basics of version control. Everyone can work on the same project and see the font growing in real-time by using the GitLab web interface. This turned out to be the smoothest workflow so far (providing that there is a local network), and it has led to surprising results done in record time.

As a cherry on top of the cake, we make use of one of GitLab's useful features — Continuous Integration (CI) — to automatically generate type specimens for each font, which are re-generated each time the font files are built.
