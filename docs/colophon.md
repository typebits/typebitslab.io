# Colophon

Type:Bits is a [Manufactura Independente](https://manufacturaindependente.org) project.

## Components

These are the free and open tools without which this vision could not have been made possible.

### Tools for font creation

* [Bitmap Font Builder](http://graphicore.de/en/archive/2010-09-09_A-Brute-Font-Attack), for converting BMF fonts into OpenType
* [FontForge](https://fontforge.github.io/), for many small operations like converting between formats and generating previews
* [fib](https://gitlab.com/foundry-in-a-box/fib), for making the FontForge operations easier to type
* [Python](https://python.org), for gluing everything together
* [Cookie Cutter](https://github.com/audreyr/cookiecutter/), for generating new fonts from a template
* [GNU Make](https://www.gnu.org/software/make/), for automating font building

### Other tools and assets

* [GitLab](http://gitlab.com), for hosting the font repositories and generating type specimens
* [MkDocs](https://mkdocs.org), for generating the website you're reading
* [League Mono](https://www.theleagueofmoveabletype.com/league-mono), the font that makes up this website

## Acknowledgements

Type:Bits was made possible by

* The host institutions of the workshops:
    - [Free Culture Forum](http://fcforum.net/), Barcelona
    - [Mozilla Festival](https://mozillafestival.org/), London
    - [Escuela de Arte Antonio Lopez](http://www.escueladeartetomelloso.org/), Tomelloso
    - [Escuela Superior de Arte del Principado de Asturias](http://esapa.org), Avilés
    - [Mur.at](http://mur.at), Graz

* [GitLab](https://gitlab.com), who provide an invaluable framework to host projects and make our font generation pipeline possible thanks to their excellent CI offering.

This documentation site could only exist thanks to the patronage of
[mur.at](https://mur.at) and their artist residency program, which allowed us
to take the time and efforts to make this web site happen.

