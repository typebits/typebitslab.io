
![Type:Bits](../assets/images/typebits-header.png)

# Libre font design workshops

Since 2013, [Manufactura Independente](http://manufacturaindependente.org) have
been developing a set of workshops dedicated to type design by means of bitmap
fonts and F/LOSS development methodologies.

The Type:Bits workshops have been hosted in
[Barcelona](http://manufacturaindependente.org/fontstxt),
[London](http://manufacturaindependente.org/blocktype-mozfest), Tomelloso,
[Avilés](http://manufacturaindependente.org/typebits) and Graz.  If you are
interested in hosting one of these, do get in touch through our e-mail address:
**hi [at] manufacturaindependente [dot] org**

## About the workshops

The main objective of each Type:Bits workshop is to collaboratively develop at
least one finished font in the course of a few hours.

To achieve this, we focus on **bitmap fonts**, which are ideal to introduce
design restraints and facilitate the explanation and application of basic type
design concepts. We also employ a custom font format that facilitates
collective design, and approach the font design process by using
non-conventional means of designing typefaces, from plain text files to games.

**Free/Libre and open source software** development practices are the base of
many of the principles behind these workshops. Not only do we exclusively
employ free/libre tools, many libre software development practices form the
base of the workshop's structure.

The backbone of what makes this collaborative design pipeline is **Git**,
namely through the GitLab platform, allowing everyone to work together and see
results immediately.

You can read more about the [principles and methodology](/principles/) behind
Type:Bits, [see and download the fonts](/fonts/) made in [past
workshops](/workshops/). There is also documentation about the more technical
aspects behind the type design workflow: the [BMF font format](/bmf-format/),
how to [start]() and [build](/building/) font files, and how [GitLab is
used](/gitlab/) to sync work and generate font files and specimens.

