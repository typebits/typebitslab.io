# Past Workshops

### Mur.at — Graz, Oct 2018

This workshop was hosted at [mur.at](https://mur.at) in Graz, as part of their artists in residency programme.

One font was produced: [Spalte](/fonts/#graz-2018).

### ESAPA — Avilés, Mar 2017

We were invited by [Escuela Superior de Arte del Principado de Asturias](http://www.esapa.org) to host two workshops as part of the [Jornadas Motiva 2017](https://www.esapa.org/2017/03/jornadas-motiva-2017-programa/).

We wrapped up with four fonts: [Muralla, Roseta, Ladrillo and Persiana](/fonts/#aviles-2017).  

[Read more about this workshop](http://manufacturaindependente.org/typebits).

> ![Aviles 01](img/workshops/aviles_01-grad.jpg)
 ![Aviles 02](img/workshops/aviles_02-grad.jpg)


### Escuela de Arte Antonio Lopez — Tomelloso, Mar 2016

This session was hosted at the [Escuela de Arte Antonio Lopez](http://escueladeartetomelloso.org) as part of the event [Volar con el Diseño &ndash; Jornadas Internacionales de Diseño](http://escueladeartevolarconeldiseno2016.blogspot.com/).

Three fonts were born here: [Avestruz, Flamingo and Tucan](/fonts/#tomelloso-2016). 


### FC Forum — Barcelona, Nov 2014

This workshop was held at [Bau, Design College of Barcelona](https://www.baued.es/), as part of the [Free Culture Forum](https://lab.2014.fcforum.net/en/).

We've created three fonts: [Setperset, Deuperset and Sisperdotze](/fonts/#barcelona-2014). 

[Read more about this workshop](http://manufacturaindependente.org/fontstxt).

> ![Barcelona 01](img/workshops/barcelona_01-grad.jpg)
 ![Barcelona 02](img/workshops/barcelona_02-grad.jpg)


### Mozfest — London, Oct 2014

We've created the Mozblock font in a 3 hour sprint at the [Mozilla Festival 2014](http://2014.mozillafestival.org/), on a session named "Design Open Web Fonts in Minecraft".

[Read more about this workshop](http://manufacturaindependente.org/blocktype-mozfest) and how Minecraft was used to make fonts.

> ![London 01](img/workshops/london_01-grad.jpg)
 ![London 02](img/workshops/london_02-grad.jpg)
