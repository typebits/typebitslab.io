# Building fonts

## Method 1: Using GitLab to automatically build font files

We recommend placing your font repository in [GitLab.com](https://gitlab.com)
in order to take advantage of their auto-build infrastructure. This means that
you will only need to push your repository to have your font files built and
made available, without having to install anything on your computer. 

The font's `README.md` already includes the download links of the generated
files.


## Method 2: Build font files in your computer

While it's handy to have GitLab build the font files for you, it takes a couple
of minutes to update the finished files after pushing. If you want to build
them locally to see your results right away, read below.


### Installing dependencies

In order to be able to build the finished font files from the source files, you
need the following dependencies installed:

* `python-virtualenv`
* FontForge and its Python bindings
* [fib](https://gitlab.com/foundry-in-a-box/fib.git)
* [bitmapfontbuilder](https://gitlab.com/typebits/bitmapfontbuilder)

First, install `python-fontforge` using your OS's package manager. This is
necessary, since it's not installable through Pip. On Debian/Ubuntu, just type

    sudo apt-get install python-fontforge

And if you don't have it, install `python-virtualenv` as well.

    sudo apt-get install python-virtualenv

Now, just run `make install` to pull and install the remaining dependencies.

Finally, clone the `bitmapfontbuilder` repository to the same directory where
you've pulled this font, so that both are inside the same folder.

    cd ..
    git clone https://gitlab.com/typebits/bitmapfontbuilder


### Building locally

Whenever you want to re-generate the font files from the source, just type

    make build

This will generate the files in FontForge (`.sfd`), TrueType (`.ttf`) and
OpenType (`.otf`) formats.

### Building and deploying to a repository

A better workflow is to create a Git repository from this font directory and
running 

    make deploy
    
This command is identical to `make build` but pulls changes from the repository
before building, generates the font files, commits the newly built files and
pushes the changes back into the remote repository.  This is particularly handy
if you're working with more people and want to incorporate their changes
quickly.

