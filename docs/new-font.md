# Creating a new font

Want to dive in the wonderful world of bitmap font drawing?

There are two available methods to quickly get started with a ready-to-edit
bare font template:

1. **Fork the `font-skeleton` repository**. Faster to get started, but needs
   some tweaking to the font's configuration. 
2. **Use the `cookiecutter-bitmapfont` template**. Requires you to install
   `cookiecutter`, but makes it easier to define the configuration values
   through a series of simple questions.

Additionally, you can try forking an existing font to have an existing base to
work from -- just follow method 1 and instead of `font-skeleton`, choose
another repository to fork from.

## Method 1: Fork the font skeleton repo

You can get started with a bare version of a bitmap font by forking the
[font-skeleton](https://gitlab.com/typebits/font-skeleton) repository on the
GitLab.com web interface.

If you want an easier start, try forking one of the existing fonts in the
[Type:Bits repository list](https://gitlab.com/typebits).

After renaming your fork to your new font's name, you need to do the following: 

1. Edit the `Skeleton-Regular.json` file to specify your font's metadata and
   properties. This file is used for generating the font files.
2. Edit the `README.md` and fill it out using your font information. This file
   is for people to read more about your font.
3. Edit the `options/options.json` file to set your font's dimensions, as
   indicated in the [BMF font format](/bmf-format/) page
4. Edit the `.gitlab-ci.yml` and change the instances of `Skeleton-Regular.XXX` to your
   font's file name. They should match your base `.json` option file name
   mentioned in step 1.
5. Edit the font name in the first line of the `LICENSE` file.


## Method 2: Use the Cookiecutter template

No need to install anything from this repository; just [install
cookiecutter](https://cookiecutter.readthedocs.io/en/latest/installation.html#install-cookiecutter)
and do:

    cookiecutter https://gitlab.com/typebits/cookiecutter-bitmapfont

You'll be asked a few questions about the font:

    font_name [Test Font]: 
    fontfile_name [TestFont]: 
    homepage []: 
    authors []: 
    lineCount [14]: 
    descent [4]: 

Here is a brief description of each of the fields:

* `font_name`: The full name for the font, including spaces.
* `fontfile_name`: The name to use for the font files, without spaces.
* `homepage`: The URL to the font's repository or site.
* `authors`: A comma-separated list of the font's authors.
* `lineCount`: The number of horizontal rows in the font's glyphs, including
  ascender and descender height.
* `descent`: The number of lines for character descenders (e.g. `p`).

You can always edit these parameters inside the base `.json` specification file
or in `options/options.json`.

## Now what?

Now that you have the base structure, it's time to create and fill out each
glyph.

Each glyph lives in its own file, and it is drawn in an ASCII-art like format,
like this:

```
..###..
.#...#.
.#...#.
.#...#.
.#####.
.#...#.
.#...#.
```

See the [BMF format](/bmf-format/) page for more information on glyphs and
option files, and once you are done designing your glyphs, check the
[Building](/building/) page to generate the final font files that you can use
in most graphics programs.

